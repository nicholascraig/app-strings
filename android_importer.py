import xml.etree.ElementTree as ET

def process_import(filename):
  texts = []
  file_contents=""
  with open(filename, "rb") as strings_xml:
    file_contents = strings_xml.read();
  resources = ET.fromstring(file_contents)
  for string_element in resources.getchildren():
    texts.append(AndroidImportText(string_element))        
  return texts

class AndroidImportText(object):
  def __init__(self, element_tree):
      self.key = element_tree.attrib['name']
      self.value = element_tree.text.replace("\\", "")
  def as_dict(self):
    return {"key":self.key, "text":self.value}

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description='')
  parser.add_argument("file_path")
  args=parser.parse_args()

  texts = process_import(args.file_path)
  for text in texts:
    print( repr(text.as_dict()))