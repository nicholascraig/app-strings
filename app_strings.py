import json
from jinja2 import Template
import os.path, sys

def _initialize_app_data(fname):
  with open(fname) as app_data_file:
    app_data_contents = app_data_file.read()
    app_data = json.loads(app_data_contents)
    return app_data

def _get_project_name(app_data):
  return app_data['config']['project_name']

def _get_texts(app_data, locale=''):
  locale = _get_locale(app_data, locale)
  texts = app_data['locales'][locale]['texts']
  # print("_get_texts returning "+repr(texts))
  return texts

def _get_locale(app_data, locale=''):
  text_locale = ''
  if(locale):
    text_locale = locale
  else:
     text_locale = _get_default_locale(app_data)
  return text_locale

def _get_default_locale(app_data):
  return app_data['config']['default_locale']

def _get_locale_and_filename(app_data, base_filename, locale, output_directory):
  locale = _get_locale(app_data, locale)
  default = _get_default_locale(app_data)
  output_file_name = base_filename
  if(locale is not default):
    output_file_name = locale + "." + output_file_name
  if output_directory:
    output_file_name = os.path.join(os.path.abspath(output_directory), output_file_name)
  return (locale, output_file_name)


def build_default_config(platform, locale='en'):
  return {"project_name":"replace with project name", "platforms":[platform], "locales":[locale], "default_locale":locale}

def build_default_master(platform, locale='en', texts=[]):
  master = {}
  master['config'] = build_default_config(platform, locale)
  master['locales'] = {}
  master['locales'][locale] = {"texts":texts}
  return master

def merge_data_to_master(master_app_data, texts, locale='en'):
  master_texts = master_app_data['locales'][locale]['texts']
  if master_texts:
    #update
    print("master_texts is true")
  else:
    print("master_texts is false")
    master_texts = texts
  master_app_data = master_app_data['locales'][locale]['texts']
  return master_app_data



''' IMPORT '''
def handleImport(args):

  if not os.path.isfile(args.import_path):
      sys.exit("No import file at "+args.import_path)

  print("importing " + args.import_path +" to "+  args.master_path)

  import android_importer
  android_import_texts = android_importer.process_import(args.import_path)
  texts = [android_import_text.as_dict() for android_import_text in android_import_texts]

  if os.path.isfile(args.master_path): #merge
    app_data=_initialize_app_data(args.master_path)
    app_data = merge_data_to_master(app_data, texts)
  else: #create for first time
    app_data=build_default_master(args.platform, args.locale, texts)

  outpath = os.path.abspath(args.master_path)  
  with open(outpath, 'w') as output:
    output.write(json.dumps(app_data, sort_keys=True,indent=4, separators=(',', ': ')))



''' EXPORT '''

class AndroidText(object):
  def __init__(self, text):
    # print('init with '+repr(text))
    if 'android_key' in text:
      self.key = text['android_key']
    else:
      self.key = text['key']

    self.value = text['text']

def _dump_android(app_data, output_directory,locale,):
  locale, output_file_name = _get_locale_and_filename(app_data, "master_strings.xml", locale, output_directory)

  print("dumping locale= {} for {} as {}".format(locale, _get_project_name(app_data), output_file_name))
  
  template_contents = ""
  with open(os.path.abspath('./templates/android.template')) as template_file:
    template_contents = template_file.read()
  template = Template(template_contents)
  android_texts = [AndroidText(text) for text in _get_texts(app_data)]
  rendered = template.render(texts=android_texts)
  with open(output_file_name, 'w') as output_file:
    output_file.write(rendered)


class IosText(object):
  def __init__(self, text):
    # print('init with '+repr(text))
    if 'ios_key' in text:
      self.key = text['ios_key']
    else:
      self.key = text['key']

    self.value = self.format_value(text['text'])
  def format_value(self, text_value):
    formatted = text_value.replace("%s", "%@")
    return formatted    

def _dump_ios(app_data, output_directory, locale):
  locale, output_file_name = _get_locale_and_filename(app_data, "master_localizable.strings", locale, output_directory)
  print("dumping locale= {} for {} as {}".format(locale, _get_project_name(app_data), output_file_name))

  template_contents = ""
  with open(os.path.abspath('./templates/ios.template')) as template_file:
    template_contents = template_file.read()
  template = Template(template_contents)
  texts = [IosText(text) for text in _get_texts(app_data)]
  rendered = template.render(texts=texts)
  with open(output_file_name, 'w') as output_file:
    output_file.write(rendered)


def handleExport(args):
  master_path = os.path.abspath(args.master_path)
  if not os.path.isfile(master_path):
      sys.exit("No master file at "+master_path)

  app_data = _initialize_app_data(master_path)

  platform = args.platform.lower()[0]
  exporter = None
  if(platform is 'a'):
    exporter= _dump_android
  elif(platform is 'i'):
    exporter= _dump_ios
  else:
    sys.exit(args.platform + " is not implemented")

  if exporter:
    exporter(app_data, args.output_directory, args.locale)



if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser(description='')
  parser.add_argument("-m", action='store',default='./example/example.json', dest='master_path', help='Path to json master text document')
  parser.add_argument("-p", default='android',  dest='platform', help="platform to generate output for. ie 'ios' or 'android'")
  parser.add_argument("-i", dest='import_path', help='path to file for import, if specified path_to_master does not exists a default file will be created. If locale is not given, "en" will be assumed')
  parser.add_argument("-l", dest='locale', default='en', help='locale to import/export as')
  parser.add_argument("-o", dest='output_directory',default='./example', help='directory to output exports to')
  args=parser.parse_args()
  print(args)

  if(args.import_path):
    handleImport(args)
  else:
    handleExport(args)

  

