# app_strings

app_strings is a command line tool for managing a localization-aware set of strings to be used across platforms. The strings are stored as a json document.

##Currently supported

- import an uncomplicated initial Android strings.xml file

- export as strings.xml

- export as localizable.strings

##Future 

- import translations to existing master


### Version
0.0.1

### Tech

app_strings uses python standard library [json] and [xml] and should run on OSX system python. it might run on three.

### Installation
NYI

### Usage
All of the operations below accept a locale that defaults to 'en'

Import Android:
```sh
$ python app_strings.py -m /path/to/save/master.json -i /path/to/strings.xml
```

Export Android:
```sh
$ python app_strings.py -m /path/to/master.json -p android
```

Export iOS:
```sh
$ python app_strings.py -m /path/to/master.json -p ios
```

### Example
The project currently defaults to 'example mode'

To see it action..

delete en.master_localizable.strings, en.master_strings.xml, example.json from ./example
import example android strings
export to ios
edit master exampl.json file

```sh
$ rm ./example/e*
$ python app_strings.py -i ./example/android_import_example.xml
$ python app_strings.py -p ios
$ python app_strings.py -p android
```

Check ./example/master_example.json for options such as custom keys/resource ids.
```sh
$ rm ./example/e*
$ python app_strings.py -m ./example/master_example.json
$ python app_strings.py -p ios
$ python app_strings.py -p android
```

### Todo's

-Import From iOS

-Import/Export Rails


[json]:https://docs.python.org/2/library/json.html
[xml]:https://docs.python.org/2/library/xml.etree.elementtree.html